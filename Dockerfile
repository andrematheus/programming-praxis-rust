FROM rust:1.31

MAINTAINER André Roque Matheus <amatheus@mac.com>

WORKDIR /usr/src/app
COPY . .

CMD [ "cargo", "test" ]
